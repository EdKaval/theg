#define FPS 60
#define SCREEN_W 1920
#define SCREEN_H 1080

#include <iostream>
#include "AllegroBase.hpp"
#include "Circle.h"
#include "Square.h"
#include "Container.h"
#include <windows.h>
#include <cstdlib>
#include <cstdio>
#include <cmath>
#include "MovingCircle.h"

using namespace std;

///////////////////////////////////////////////////////////////////////////

Container cont(20);
MovingCircle* player1;

class thebase: public AllegroBase {
public:
    virtual void Fps() {  //Update
        cont.Update();
    }
    virtual void Draw() {
        al_clear_to_color( al_map_rgb( 0, 0, 0 ) );

        cont.Draw();
    }
    virtual void OnKeyDown( const ALLEGRO_KEYBOARD_EVENT &keyboard ) {
        if ( keyboard.keycode == ALLEGRO_KEY_SPACE )
        {
        }
        else if ( keyboard.keycode == ALLEGRO_KEY_ESCAPE )
        {
            Exit();
        }
        if ( keyboard.keycode == ALLEGRO_KEY_UP ) {
            player1->DoIt(1);
        }
        if ( keyboard.keycode == ALLEGRO_KEY_DOWN ) {
            player1->DoIt(2);
        }
        if ( keyboard.keycode == ALLEGRO_KEY_LEFT ) {
            player1->DoIt(3);
        }
        if ( keyboard.keycode == ALLEGRO_KEY_RIGHT ) {
            player1->DoIt(4);
        }
        if ( keyboard.keycode == ALLEGRO_KEY_LSHIFT ) {
            player1->DoIt(5);
        }
        cout << "aaaaa" << endl;
    }
};

//////////////////////////////////////////////////////////////////////////

thebase base;

int main(int argc, char **argv) {

    player1 = new MovingCircle;
    cont.Add(player1);

    if (!base.Init(SCREEN_W, SCREEN_H, FPS)) {
        return 0;
    }
    base.Run();
    return 0;
}
