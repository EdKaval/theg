#pragma once
#include "Figure.h"
#include "Circle.h"
#include "Square.h"

class FigureFactory {
public:
    static Figure* RandomCircle();
    static Figure* RandomSquare();
};


