#include "Square.h"

Square::Square(double xx, double yy, double lx, double ly):
    Figure(xx, yy, 10.0 - (rand() % 210) / 10.0, 10.0 - (rand() % 210) / 10.0),
    len(lx, ly),
    color(rand() % 255, rand() % 255, rand() % 255)
{
}

void Square::Update() {
    pos.x += dpos.x;
    pos.y += dpos.y;
    if (( pos.x-len.x/2 <= 0.0 ) || ( pos.x+len.x/2 >= SCREEN_W )) {
        dpos.x = -dpos.x;
    }
    if ((pos.y-len.y/2 <= 0.0) || (pos.y+len.y/2 >= SCREEN_H)) {
        dpos.y = -dpos.y;
    }
}

void Square::Draw() const {
    al_draw_filled_rectangle( pos.x-len.x/2, pos.y-len.y/2, pos.x+len.x/2, pos.y+len.y/2, al_map_rgb( color.x, color.y, color.z ) );
}

double Square::getCollisionRadius() const {
    return len.mod()/2;
}
