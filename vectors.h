#pragma once

class vec2 {
public:
    double x, y;
    vec2(double xx, double yy);
    double mod() const;
};

class vec3 {
public:
    double x, y, z;
    vec3(double xx, double yy, double zz);
};

class vec4 {
public:
    double x, y, z ,w;
    vec4(double xx, double yy, double zz, double ww);
};

double getDist(vec2 a, vec2 b);
