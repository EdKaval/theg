#pragma once
#include "Figure.h"

class Square: public Figure {
protected:
    vec2 len;
    vec3 color;
public:
    Square(double xx, double yy, double lx, double ly);
    virtual void Update();
    virtual void Draw() const;
    virtual double getCollisionRadius() const;
};
