#include "FigureFactory.h"

Figure* FigureFactory::RandomCircle() {
    return new Circle();
}

Figure* FigureFactory::RandomSquare() {
    return new Square(rand() % SCREEN_W, rand() % SCREEN_W, 140, 20);
}
