#pragma once
#include "Figure.h"

class Circle: public Figure {
protected:
    vec3 color;
    double r;
public:
    Circle();
    virtual ~Circle(){}
    virtual void Update();
    virtual void Draw() const;
    virtual double getCollisionRadius() const;
};
