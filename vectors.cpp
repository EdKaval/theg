#include "vectors.h"
#include <cmath>

vec2::vec2(double xx, double yy):
    x(xx),
    y(yy)
{
}

double vec2::mod() const {
    return sqrt((x*x) + (y*y));
}

vec3::vec3(double xx, double yy, double zz):
    x(xx),
    y(yy),
    z(zz)
{
}

vec4::vec4(double xx, double yy, double zz, double ww):
    x(xx),
    y(yy),
    z(zz),
    w(ww)
{
}

double getDist(vec2 a, vec2 b) {
    return sqrt((b.x - a.x)*(b.x - a.x) + (b.y - a.y)*(b.y - a.y));
}
