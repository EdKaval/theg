#pragma once
#include "vectors.h"
#include "AllegroBase.hpp"

#define SCREEN_W 1920
#define SCREEN_H 1080

class Figure {
protected:
    vec2 pos;
    vec2 dpos;
public:
    Figure(double xx, double yy, double dx, double dy);
    virtual void Update() = 0;
    virtual void Draw() const = 0;
    virtual void Collide(vec2 apos);
    virtual vec2 getCoords() const;
    virtual vec2 getDpos() const;
    virtual double getCollisionRadius() const = 0;
    virtual ~Figure() {}
};
