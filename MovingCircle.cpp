#include "MovingCircle.h"
#include <iostream>
#include "AllegroBase.hpp"
#include <windows.h>
#include <cstdlib>
#include <cstdio>

void MovingCircle::DoIt(int key) {
    if ( key == 1 ) {
        dpos.y = -1;
    }
    if ( key == 2 ) {
        dpos.y = +1;
    }
    if ( key == 3 ) {
        dpos.x = -1;
    }
    if ( key == 4 ) {
        dpos.x = +1;
    }
    if ( key == 5 ) {
        dpos.y *= 10;
        dpos.x *= 10;
    }
}

//MovingCircle::MovingCircle() {

//}
