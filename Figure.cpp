#include "Figure.h"
#include <cmath>

Figure::Figure(double xx, double yy, double dx, double dy):
    pos(xx, yy),
    dpos(dx, dy)
{
}

vec2 Figure::getCoords() const {
    return pos;
}

vec2 Figure::getDpos() const {
    return dpos;
}

void Figure::Collide(vec2 apos) {
    double lnn = dpos.mod();
    dpos.x = lnn*cos( acos((apos.y - pos.y)/getDist(apos, pos)));
    dpos.y = lnn*sin( acos((apos.y - pos.y)/getDist(apos, pos)));
}
