#include "Circle.h"


Circle::Circle():
    Figure(rand() % SCREEN_W, rand() % SCREEN_H, 10.0 - (rand() % 210) / 10.0, 10.0 - (rand() % 210) / 10.0),
    r(10.0 + rand() % 30),
    color(rand()%255, rand()%255, rand()%255)
{
}

void Circle::Update() {
    pos.x += dpos.x;
    pos.y += dpos.y;
    if ( pos.x + r < 0.0 ) {
        pos.x = SCREEN_W+r;
        pos.y = SCREEN_H-pos.y;
    }
    if ( pos.x - r > SCREEN_W ) {
        //dpos.x = -dpos.x;
        pos.x = 0-r;
        pos.y = SCREEN_H-pos.y;
    }
    if (pos.y + r < 0.0) {
        pos.y = SCREEN_H+r;
        pos.x = SCREEN_W-pos.x;
    }
    if (pos.y - r > SCREEN_H) {
        //dpos.y = -dpos.y;
        pos.y = 0-r;
        pos.x = SCREEN_W-pos.x;
    }
}

void Circle::Draw() const {
    al_draw_filled_circle(pos.x, pos.y, r, al_map_rgb( color.x, color.y, color.z ));
}

double Circle::getCollisionRadius() const {
    return r;
}
